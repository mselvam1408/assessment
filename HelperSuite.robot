*** Settings ***
Resource          Variables.robot
Library           SeleniumLibrary
Resource          Locators.robot

*** Keywords ***
Login_With_Username_Password
    [Arguments]    ${argument_username}    ${argument_password}
    Input Text    ${locator_username_textbox}    ${argument_username}
    Input Password    ${locator_password_textbox}    ${argument_password}
