*** Keywords ***
Login_With_Username_Password
    [Arguments]    ${variable_username}    ${variable_password}
    Input Text    ${locator_username_textbox}    ${variable_valid_username}
    Input Password    ${locator_password_textbox}    ${variable_valid_password}
