*** Settings ***
Library           SeleniumLibrary
Resource          Variables.robot
Resource          Locators.robot
Resource          HelperSuite.robot

*** Test Cases ***
LoginWithValidCredentials
    [Documentation]    Login with valid username and password.
    ...    Test data: username=tomsmith; password=SuperSecretPassword!
    [Tags]    Functional    Smoke
    Open Browser    ${variable_application_url}    ${variable_browser}
    Maximize Browser Window
    Login_With_Username_Password    ${variable_valid_username}    ${variable_valid_password}
    Capture Page Screenshot
    Click Button    ${locator_login_button}
    Wait Until Element Is Visible    ${locator_logout_button}    10s    ${variable_element_not available}
    ${argument_get_login_message}=    Get Text    ${locator_sucessful_message}
    Log    ${argument_get_login_message}
    Should Be Equal    ${argument_get_login_message}    ${variable_successful_login_message}
    Capture Page Screenshot
    Click Element    ${locator_logout_button}
    [Teardown]    Close Browser
