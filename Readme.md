

## Prerequisite to run Robot Framework: 

- Install Python version 3.x and set the Python path
- Install the below libraries by using pip commands:
    a. ```pip install robotframework-seleniumlibrary```
    b.```pip install robotframework```
- Make sure to install pycharm/IntelliJ with python and robot framework plugin.
- Command to run test with output file in specific folder 
    ```robot --outputdir Outputdirectory TestAutomation.robot```

> Notes: 

- Screenshots are added for all the screens.
- On failure test cases screen shots will be generated from the framework.
- Log Report will be generated once the test is completed. 

